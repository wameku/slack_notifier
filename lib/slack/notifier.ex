defmodule Slack.Notifier do
  use GenServer
  @behaviour WamekuNotifier

  @ok "#36a64f"
  @critical "#ff0000"
  @warning "#f9cc10"
  @unknown "#fffff"

  def start_link(args \\ []) do
    GenServer.start_link(__MODULE__, args, [name: __MODULE__])
  end

  def init(args) do
    {:ok, args}
  end

  def notify(message, config) do
    case :httpc.request(:post, {url(config), [], 'application/json', payload(message, config)}, [], []) do
      {:ok, {{'HTTP/1.1', 200, _state}, _head, _body}} ->
        {:ok, "message sent"}
      error ->
        {:error, error}
    end
  end

  def status(%{recovery: true}) do
    "Recovered"
  end
  def status(%{recovery: false}=message) do
    case message.exit_code do
      0 -> "OK"
      1 -> "WARNING"
      2 -> "CRITICAL"
      _unknown -> "UNKNOWN"
    end
  end
  def status_color(message) do
    case message.exit_code do
      0 -> @ok
      1 -> @warning
      2 -> @critical
      _unknown -> @unknown
    end
  end
  def build_text(message) do
    ["Status: #{status(message)}", "Check Name: #{name(message)}", "Output: #{output(message)}", "When: <!date^#{last_checked(message)}^{date_num} {time_secs}|Could not parse date>", "Exit Code: #{exit_code(message)}"]
  end
  def name(message) do
    message.name
  end
  def output(message) do
    message.output
  end
  def host(message) do
    message.host
  end
  def last_checked(message) do
    message.last_checked
  end
  def exit_code(message) do
    message.exit_code
  end
  def icon_emoji(config) do
    if Map.has_key?(config, :icon_emoji) do
      config.icon_emoji
    else
      ":bell:"
    end
  end
  def payload(message, config) when is_map(message) do
    Poison.encode!(%{"icon_emoji" => icon_emoji(config), "channel" =>  config.channel, "username" => config.username, "text" => "Wamkeu Check", "attachments" => [%{"author_icon" => "http://beta.wameku.com/server-icon.png", "author_name" => "#{host(message)}", "text" => Enum.join(build_text(message), "\n"), "color" => status_color(message), "footer": "Wameku"}]})
  end
  def url(config) do
    to_char_list(config.url)
  end
end
