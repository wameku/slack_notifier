defmodule Slack do
  use Application

  # See http://elixir-lang.org/docs/stable/elixir/Application.html
  # for more information on OTP Applications
  def start(_type, _args) do
    import Supervisor.Spec, warn: false
    :resource_discovery.add_local_resource_tuple({:notifiers, {:slack, node()}})
    :resource_discovery.sync_resources()

    children = [
      worker(Slack.Notifier, []),
    ]

    # for other strategies and supported options
    opts = [strategy: :one_for_one, name: Slack.Supervisor]
    Supervisor.start_link(children, opts)
  end
end
