use Mix.Config

config :slack,
  url: "${SLACK_URL}",
  channel: "${SLACK_CHANNEL}",
  username: "${SLACK_USERNAME}"

